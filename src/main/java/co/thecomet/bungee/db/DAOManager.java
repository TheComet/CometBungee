package co.thecomet.bungee.db;

import co.thecomet.db.mongodb.ResourceManager;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

import java.util.HashMap;
import java.util.Map;

public class DAOManager {
    private static Map<Class, BasicDAO<?, ObjectId>> daoStore = new HashMap<>();
    private static Datastore datastore;

    public DAOManager() {
        datastore = ResourceManager.getInstance().getDatastore();
    }

    public static <T> BasicDAO<T, ObjectId> getDAO(Class<T> clazz) {
        BasicDAO<T, ObjectId> dao = daoStore.containsKey(clazz) ? (BasicDAO<T, ObjectId>) daoStore.get(clazz) : registerDBClass(clazz);
        return dao;
    }

    protected static <T> BasicDAO<T, ObjectId> registerDBClass(Class<T> clazz) {
        BasicDAO<T, ObjectId> dao = new BasicDAO<>(clazz, datastore);
        datastore.ensureIndexes();
        datastore.ensureCaps();
        daoStore.put(clazz, dao);
        return dao;
    }
}
