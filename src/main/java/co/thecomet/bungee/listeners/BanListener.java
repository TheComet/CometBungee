package co.thecomet.bungee.listeners;

import co.thecomet.bungee.BungeeHelper;
import co.thecomet.bungee.db.DAOManager;
import co.thecomet.bungee.db.entities.ModerationEntry;
import co.thecomet.bungee.db.entities.ModerationHistory;
import co.thecomet.bungee.db.entities.User;
import co.thecomet.bungee.utils.ModerationUtils;
import co.thecomet.bungee.utils.TimeFormatUtils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;
import org.bson.types.ObjectId;
import org.mongodb.morphia.dao.BasicDAO;

public class BanListener implements Listener {
    @EventHandler
    public void onLogin(final LoginEvent event) {
        Plugin plugin = BungeeHelper.getInstance();

        event.registerIntent(plugin);
        ProxyServer.getInstance().getScheduler().runAsync(plugin, () -> {
            BasicDAO<User, ObjectId> dao = DAOManager.getDAO(User.class);
            User user = dao.findOne("uuid", event.getConnection().getUniqueId().toString());

            if (user != null && user.moderationHistory != null) {
                ModerationHistory history = user.moderationHistory;

                if (ModerationUtils.isBanned(history)) {
                    ModerationEntry ban = history.activeBan;

                    event.setCancelled(true);
                    if (ban.expiration == null) {
                        event.setCancelReason(ChatColor.translateAlternateColorCodes('&', "\n&cYou have been banned from this network!"
                                + "\n&6Reason: &c" + ban.reason));
                    } else {
                        long duration = ban.expiration.getTime() - System.currentTimeMillis();
                        event.setCancelReason(ChatColor.translateAlternateColorCodes('&', "\n&cYou are temporarily banned from the network!"
                                + "\n&6Reason: &c" + ban.reason
                                + "\n&6You will be unbanned in &c" + TimeFormatUtils.convertToFormattedTime(duration, false)));
                    }
                }
            }

            event.completeIntent(plugin);
        });
    }
}
