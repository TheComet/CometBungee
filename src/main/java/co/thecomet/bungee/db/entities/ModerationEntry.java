package co.thecomet.bungee.db.entities;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

import java.util.Date;

@Entity(value = "network_users_moderation_history_entries", noClassnameStored = true)
public class ModerationEntry {
    @Id
    public ObjectId id;
    @Indexed
    public String uuid;
    @Indexed
    public String staffUuid;
    @Indexed
    public ModerationAction action;
    public String reason;
    public Date createdDate;
    public Date expiration;

    public ModerationEntry(String uuid, String staffUuid, ModerationAction action, String reason, long expiration) {
        this.uuid = uuid;
        this.staffUuid = staffUuid;
        this.action = action;
        this.reason = reason;
        this.createdDate = new Date(System.currentTimeMillis());
        this.expiration = expiration > 0 ? new Date(System.currentTimeMillis() + expiration) : null;
    }

    public ModerationEntry(String uuid, String staffUuid, ModerationAction action, String reason) {
        this(uuid, staffUuid, action, reason, -1);
    }

    public ModerationEntry() {}
}
