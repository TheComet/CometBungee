package co.thecomet.bungee.db.entities;

import co.thecomet.common.user.Rank;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

@Entity(value = "network_users", noClassnameStored = true)
public class User {
    @Id
    public ObjectId id;
    @Indexed(unique = true)
    public String uuid;
    public String nameLower;
    public Rank rank = Rank.DEFAULT;
    public ModerationHistory moderationHistory = new ModerationHistory();

    public User() {}
}
