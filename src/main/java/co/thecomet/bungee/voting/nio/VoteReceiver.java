package co.thecomet.bungee.voting.nio;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.TimeUnit;

import javax.crypto.BadPaddingException;

import co.thecomet.bungee.voting.VoteManager;
import co.thecomet.bungee.voting.crypto.RSA;
import co.thecomet.bungee.voting.model.Vote;
import co.thecomet.bungee.voting.model.VoteEvent;

/**
 * The vote receiving server.
 */
public class VoteReceiver implements Runnable {
    private final VoteManager manager;

    /**
     * The host to listen on.
     */
    private final String host;

    /**
     * The port to listen on.
     */
    private final int port;

    /**
     * The server socket.
     */
    private ServerSocket server;

    /**
     * The running flag.
     */
    private boolean running = true;

    /**
     * Instantiates a new vote receiver.
     *
     * @param host The host to listen on
     * @param port The port to listen on
     */
    public VoteReceiver(final VoteManager manager, String host, int port) throws Exception {
        this.manager = manager;
        this.host = host;
        this.port = port;

        initialize();
    }

    private void initialize() throws Exception {
        try {
            server = new ServerSocket();
            server.bind(new InetSocketAddress(host, port));
        } catch (Exception ex) {
            manager.getLogger().severe("Error initializing vote receiver. Please verify that the configured");
            manager.getLogger().severe("IP address and port are not already in use. This is a common problem");
            manager.getLogger().severe("with hosting services and, if so, you should check with your hosting provider." +
                    ex);
            throw new Exception(ex);
        }
    }

    /**
     * Shuts the vote receiver down cleanly.
     */
    public void shutdown() {
        running = false;
        if (server == null)
            return;
        try {
            server.close();
        } catch (Exception ex) {
            manager.getLogger().warning("Unable to shut down vote receiver cleanly.");
        }
    }

    public void run() {
        BufferedWriter writer = null;
        InputStream in = null;
        Socket socket = null;

        // Main loop.
        while (running) {
            try {
                socket = server.accept();
                socket.setSoTimeout(3000); // Don't hang on slow connections.
                writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                in = socket.getInputStream();

                // Send them our version.
                writer.write("VOTIFIER " + manager.getVersion());
                writer.newLine();
                writer.flush();

                // Read the 256 byte block.
                byte[] block = new byte[256];
                in.read(block, 0, block.length);

                // Decrypt the block.
                block = RSA.decrypt(block, manager.getKeyPair()
                        .getPrivate());
                int position = 0;

                // Perform the opcode check.
                String opcode = readString(block, position);
                position += opcode.length() + 1;
                if (!opcode.equals("VOTE")) {
                    // Something went wrong in RSA.
                    throw new Exception("Unable to decode RSA");
                }

                // Parse the block.
                String serviceName = readString(block, position);
                position += serviceName.length() + 1;
                String username = readString(block, position);
                position += username.length() + 1;
                String address = readString(block, position);
                position += address.length() + 1;
                String timeStamp = readString(block, position);
                position += timeStamp.length() + 1;

                // Create the vote.
                final Vote vote = new Vote(serviceName, username, address, timeStamp);

                // Call event in a synchronized fashion to ensure that the
                // custom event runs in the
                // the main server thread, not this one.
                manager.getPlugin().getProxy().getScheduler()
                        .schedule(manager.getPlugin(), () -> manager.getPlugin().getProxy().getPluginManager()
                                .callEvent(new VoteEvent(vote)), 0, TimeUnit.SECONDS);

                // Clean up.
                writer.close();
                in.close();
                socket.close();
            } catch (SocketException ex) {
                manager.getLogger().warning("Protocol error. Ignoring packet - "
                        + ex.getLocalizedMessage());
            } catch (BadPaddingException ex) {
                manager.getLogger().warning("Unable to decrypt vote record. Make sure that that your public key");
                manager.getLogger().warning("matches the one you gave the server list." + ex);
            } catch (Exception ex) {
                manager.getLogger().warning("Exception caught while receiving a vote notification" +
                        ex);
            } finally {
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * Reads a string from a block of data.
     *
     * @param data The data to read from
     * @return The string
     */
    private String readString(byte[] data, int offset) {
        StringBuilder builder = new StringBuilder();
        for (int i = offset; i < data.length; i++) {
            if (data[i] == '\n')
                break; // Delimiter reached.
            builder.append((char) data[i]);
        }
        return builder.toString();
    }
}