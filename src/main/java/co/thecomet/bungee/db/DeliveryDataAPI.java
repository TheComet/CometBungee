package co.thecomet.bungee.db;

import co.thecomet.bungee.db.entities.DeliveryType;
import co.thecomet.bungee.db.entities.User;
import co.thecomet.bungee.db.entities.UserDeliveryStats;
import org.bson.types.ObjectId;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.UpdateOperations;

import java.util.Date;

public class DeliveryDataAPI {
    private static final int VOTE_POINTS = 500;

    public static boolean addVote(String username) {
        User user = getUser(username);
        if (user == null) {
            return false;
        } else {
            UpdateOperations<User> ops = DAOManager.getDAO(User.class).createUpdateOperations();
            ops.disableValidation();
            ops.inc("points", VOTE_POINTS);
        }

        BasicDAO<UserDeliveryStats, ObjectId> dao = DAOManager.getDAO(UserDeliveryStats.class);
        UserDeliveryStats stats = dao.findOne("uuid", user.uuid);

        if (stats == null) {
            stats = new UserDeliveryStats(user.uuid);
        }

        Date date = new Date(System.currentTimeMillis());
        stats.getStats().put(DeliveryType.VOTE_MSO, date);

        dao.save(stats);
        return true;
    }

    private static User getUser(String username) {
        User user = DAOManager.getDAO(User.class).findOne("nameLower", username.toLowerCase());

        if (user != null) {
            return user;
        }

        return null;
    }
}
