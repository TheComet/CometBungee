package co.thecomet.bungee.utils;

import co.thecomet.bungee.db.entities.ModerationEntry;
import co.thecomet.bungee.db.entities.ModerationHistory;

import java.util.Date;

public class ModerationUtils {
    public static boolean isBanned(ModerationHistory history) {
        ModerationEntry ban = history.activeBan;
        if (ban != null) {
            if (ban.expiration != null) {
                if (new Date(System.currentTimeMillis()).before(ban.expiration)) {
                    return true;
                }
            } else {
                return true;
            }
        }
        return false;
    }
}
