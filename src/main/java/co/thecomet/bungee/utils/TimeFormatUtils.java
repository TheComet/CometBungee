package co.thecomet.bungee.utils;

import java.util.concurrent.TimeUnit;

public class TimeFormatUtils {
    /**
     * Gets the unban time as a long
     *
     * @param duration the duration of the ban
     *
     * @return the duration of the ban as a long
     */
    public static Long getTime(String duration) throws NumberFormatException {
        long time = -1;
        if (duration.endsWith("s")) {
            time = TimeUnit.SECONDS.toMillis(Integer.parseInt(duration.substring(0, duration.length() - 1)));
        } else if (duration.endsWith("m")) {
            time = TimeUnit.MINUTES.toMillis(Integer.parseInt(duration.substring(0, duration.length() - 1)));
        } else if (duration.endsWith("h")) {
            time = TimeUnit.HOURS.toMillis(Integer.parseInt(duration.substring(0, duration.length() - 1)));
        } else if (duration.endsWith("d")) {
            time = TimeUnit.DAYS.toMillis(Integer.parseInt(duration.substring(0, duration.length() - 1)));
        }
        return time;
    }

    /**
     * Converts a duration to a formatted string in the format: days, hours, minuts, seconds
     *
     * @param duration duration length in milliseconds
     *
     * @return formatted time as string
     */
    public static String convertToFormattedTime(long duration, boolean shortNotation) {
        int days = (int) TimeUnit.MILLISECONDS.toDays(duration);
        int hours = (int) (TimeUnit.MILLISECONDS.toHours(duration) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(duration)));
        int minutes = (int) (TimeUnit.MILLISECONDS.toMinutes(duration) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(duration)));
        int seconds = (int) (TimeUnit.MILLISECONDS.toSeconds(duration) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration)));

        String message = "";
        if (days > 0) {
            message += String.format("%d%s", days, (shortNotation ? "d" : " days"));
        }
        if (hours > 0) {
            if (!message.equals("")) {
                message += ", ";
            }
            message += String.format("%d%s", hours, (shortNotation ? "h" : " hours"));
        }
        if (minutes > 0) {
            if (!message.equals("")) {
                message += ", ";
            }
            message += String.format("%d%s", minutes, (shortNotation ? "m" : " minutes"));
        }
        if (seconds > 0) {
            if (!message.equals("")) {
                message += ", ";
            }
            message += String.format("%d%s", seconds, (shortNotation ? "s" : " seconds"));
        }

        return message;
    }
}
