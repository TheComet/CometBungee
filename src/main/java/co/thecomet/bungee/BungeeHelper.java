package co.thecomet.bungee;

import co.thecomet.bungee.db.DAOManager;
import co.thecomet.bungee.listeners.BanListener;
import co.thecomet.bungee.listeners.VoteListener;
import co.thecomet.bungee.voting.VoteManager;
import co.thecomet.common.chat.FontColor;
import co.thecomet.redis.bungee.CRBungee;
import co.thecomet.redis.redis.pubsub.NetTaskSubscribe;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.HashMap;

public class BungeeHelper extends Plugin {
    private static BungeeHelper instance;
    private static DAOManager daoManager;
    private static RedisHandler handler;
    private static VoteManager voteManager;
    
    public void onEnable() {
        instance = this;
        daoManager = new DAOManager();

        ProxyServer.getInstance().getPluginManager().registerListener(this, new BanListener());
        ProxyServer.getInstance().setReconnectHandler(new HubBalancer());
        
        handler = new RedisHandler();
        voteManager = new VoteManager(this);
        getProxy().getPluginManager().registerListener(this, new VoteListener());
    }

    public static RedisHandler getHandler() {
        return handler;
    }

    public class RedisHandler {
        private CRBungee redis;
        
        public RedisHandler() {
            redis = CRBungee.getInstance();
            
            init();
        }
        
        private void init() {
            redis.getRedis().registerChannel("galert");
            redis.getRedis().registerTask(this);
        }
        
        @NetTaskSubscribe(name = "galert", args = {"message"})
        public void galert(HashMap<String, Object> args) {
            for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
                player.sendMessage(ChatMessageType.CHAT, new TextComponent(FontColor.translateString(String.format(getFormat(), String.valueOf(args.get("message"))))));
            }
        }
        
        private String getFormat() {
            return "&8[&c&lALERT&r&8] &r%s";
        }
    }

    public static BungeeHelper getInstance() {
        return instance;
    }
}
