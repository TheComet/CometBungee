package co.thecomet.bungee.db.entities;

public enum ModerationAction {
    BAN,
    IPBAN,
    KICK,
    MUTE;
}
